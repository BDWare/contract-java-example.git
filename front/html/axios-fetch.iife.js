var axiosFetch = (function (exports) {
    'use strict';

    var src = {};

    var typeUtils = {};

    Object.defineProperty(typeUtils, "__esModule", { value: true });
    typeUtils.getUrl = typeUtils.createAxiosHeaders = typeUtils.createFetchHeaders = void 0;
    function createFetchHeaders(axiosHeaders = {}) {
        const headers = [];
        Object.entries(axiosHeaders).forEach(([name, value]) => {
            headers.push([name, value]);
        });
        return headers;
    }
    typeUtils.createFetchHeaders = createFetchHeaders;
    const isHeaders = (headers) => { var _a; return ((_a = headers.constructor) === null || _a === void 0 ? void 0 : _a.name) === 'Headers'; };
    function createAxiosHeaders(headers = {}) {
        const rawHeaders = {};
        if (isHeaders(headers)) {
            headers.forEach((value, name) => {
                rawHeaders[name] = value;
            });
        }
        else if (Array.isArray(headers)) {
            headers.forEach(([name, value]) => {
                if (value) {
                    rawHeaders[name] = value;
                }
            });
        }
        else {
            Object.entries(headers).forEach(([name, value]) => {
                if (value) {
                    rawHeaders[name] = value;
                }
            });
        }
        return rawHeaders;
    }
    typeUtils.createAxiosHeaders = createAxiosHeaders;
    function getUrl(input) {
        let url;
        if (typeof input === 'string') {
            url = input;
        }
        else if (input === null || input === void 0 ? void 0 : input.href) {
            url = input.href;
        }
        else if (input === null || input === void 0 ? void 0 : input.url) {
            url = input.url;
        }
        return url;
    }
    typeUtils.getUrl = getUrl;

    Object.defineProperty(src, "__esModule", { value: true });
    exports.buildAxiosFetch = src.buildAxiosFetch = void 0;
    const typeUtils_1 = typeUtils;
    /**
     * A Fetch WebAPI implementation based on the Axios client
     */
    const axiosFetch = (axios, 
    // Convert the `fetch` style arguments into a Axios style config
    transformer = (config) => config) => async (input, init) => {
        const rawHeaders = (0, typeUtils_1.createAxiosHeaders)(init === null || init === void 0 ? void 0 : init.headers);
        const lowerCasedHeaders = {};
        Object.entries(rawHeaders).forEach(([name, value]) => {
            lowerCasedHeaders[name.toLowerCase()] = value;
        });
        if (!('content-type' in lowerCasedHeaders)) {
            lowerCasedHeaders['content-type'] = 'text/plain;charset=UTF-8';
        }
        const rawConfig = {
            url: (0, typeUtils_1.getUrl)(input),
            method: (init === null || init === void 0 ? void 0 : init.method) || 'GET',
            data: init === null || init === void 0 ? void 0 : init.body,
            headers: lowerCasedHeaders,
            // Force the response to an arraybuffer type. Without this, the Response
            // object will try to guess the content type and add headers that weren't in
            // the response.
            // NOTE: Don't use 'stream' because it's not supported in the browser
            responseType: 'arraybuffer'
        };
        const config = transformer(rawConfig, input, init);
        let result;
        try {
            result = await axios.request(config);
        }
        catch (err) {
            if (err.response) {
                result = err.response;
            }
            else {
                throw err;
            }
        }
        return new Response(result.data, {
            status: result.status,
            statusText: result.statusText,
            headers: (0, typeUtils_1.createFetchHeaders)(result.headers)
        });
    };
    const buildAxiosFetch = (axios, transformer) => axiosFetch(axios, transformer);
    exports.buildAxiosFetch = src.buildAxiosFetch = buildAxiosFetch;

    exports["default"] = src;

    Object.defineProperty(exports, '__esModule', { value: true });

    return exports;

})({});
