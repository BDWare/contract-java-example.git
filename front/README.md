# contract-java-example 前端开发说明

前端与后端支持分离开发。注意，启动好docker与后端之后，可单独对前端进行调试和开发。
注意在单独开发前端时，需将后端地址设置为可配置的选项。
当前端代码与后端代码打包到一起之后，前端中的`./index.html`文件会映射为`http://host:port/DOIP/ContractExample/assets/index.html`。
因此，后端的地址可通过以下代码计算得出：
```javascript
var url = document.location.href.replaceAll(/\/DOIP.*$/g,'/SCIDE')
// 对应后端地址为：url = 'http://host:port/SCIDE';
```
前端代码成功打包之后，可通过以下路径分访问前端页面。
```json
http://127.0.0.1:21030/DOIP/ContractExample/assets/vite/dist/index.html
http://127.0.0.1:21030/DOIP/ContractExample/assets/html/index.html
```


## 使用html/js
见`html/index.html`。
引用sdk及其依赖的js文件。
```
    <script src="./cryptico.iife.js"></script>
    <script src="./sm2.js"></script>
    <script src="./axios-fetch.iife.js"></script>
    <script src="./axios.min.js"></script>
    <script src="./bdcontract-sdk.iife.js"></script>
```

### 调试与开发
可在本地安装npm，利用`npm install -g http-server`来安装一个简单的httpserver。
在命令行中可启动该服务器，托管前端代码。
```
cd ./front/html
http-server
```
后续仅需修改`front/html`下的前端文件即可。


## 使用ts+vite等框架开发前端
[前端sdk](https://www.npmjs.com/package/@bdware/bdcontract-sdk) 在npm仓库中。
调用示例参考：`front/vite/src/main.ts`。

```javascript
const url = 'http://127.0.0.1:21030/SCIDE';
var sm2Key = {"publicKey": "04180354fdb6507f8ab98ccfbe165ce11da74ba733f81af86ad6d32216b32cf4f797c559d50ceeefbf4c760c3483840471c67471b90acdffb388cd7d496d9a1610",
               "privateKey": "1d4196947f59532db6f8f4055e58474a48db8f30b476ae3edc66406464521b3b"};
const client = new HttpClient(url, sm2Key);
const data = await client.executeContract(
    'ContractExample',
    'callHello',
    'abc');
alert(data.data);
```
### 调试与开发
打开命令行，在`front/vite`目录下，使用`npm run dev`，开启调试，
打开按命令行中的提示地下，打开浏览器页面，如`http://localhost:3000/`即可。

