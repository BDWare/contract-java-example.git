import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

export default defineConfig({
  base: './',             // 设开发或生产环境服务的 公共基础路径
  define: {              // 类型： Record<string, string> 定义全局变量替换方式。每项在开发时会被定义为全局变量，而在构建时则是静态替换。
    'process.platform': null,
    'process.version': null,
  },
  plugins: [           // 类型： (Plugin | Plugin[])[]  将要用到的插件数组
    vue()
  ],
 });
