# 镜像准备
请提前熟悉docker常用命令，主要包括`docker ps`、`docker image xx`、`docker kill`等。
1. 下载或导入镜像：
```bash
#下载镜像
docker pull bdware/bdcontract:latest
#导入镜像，bdcontract-x.x.x.tar为待导入镜像文件。
docker load -i bdcontract-x.x.x.tar
```
2. 检查是否有"bdware/bdcontract:latest"镜像。
```bash
docker image ls | grep bdcontract
```
如果没有名为"bdware/bdcontract latest"的镜像，就将其中的最新版本打上。
docker image tag bdware/bdcontract:x.y.z bdware/bdcontract:latest

# 修改配置
1. 生成管理员密钥对。 将公钥配置到manager.key中。 格式为单行130个字符。类似于
```
04dad765858...
```

也可利用cp目录下的jar包自己生成:
```bash
#本操作需要依赖java 1.8以上环境。
java -cp cp/libs:cp/yjs.jar org.bdware.sc.SM2Helper generateKeyToFile
```
生成的文件请妥善保存。

2. 修改配置文件cmconfig.json（如果需要），配置文件参数详见配置说明`cmconfig.readme.md`。 

3. 执行脚本`sh start.sh`