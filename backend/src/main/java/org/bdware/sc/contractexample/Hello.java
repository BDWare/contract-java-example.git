package org.bdware.sc.contractexample;

import com.google.gson.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.sc.ContractProcess;
import org.bdware.sc.boundry.JavaScriptEntry;
import org.bdware.sc.engine.JSONTool;
import wrp.jdk.nashorn.api.scripting.ScriptObjectMirror;

public class Hello {
    public static String call(ScriptObjectMirror obj) {
        JsonObject jo = JSONTool.convertMirrorToJson(obj).getAsJsonObject();
        //...
        return "0320-hello";
    }
    private static final Logger LOGGER = LogManager.getLogger(Hello.class);

    public static void main(String[] args) {
        LOGGER.info("abc");
    }

    public static String call() {
        return "0320-hello";
    }



    public static String callYJSInSameCP() {
        return ContractProcess.instance.executeContract("{\"action\":\"getOwner\"\",\"arg\":\"\"}");
    }

    public static Object callYJSInOtherCP() {
        return JavaScriptEntry.executeContract("cid", "action", "arg");
    }
}